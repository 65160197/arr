public class ListInsertionSortApp {

    public static void main(String[] args) {
        int size = 10;
        // create an array of links
        Link[] linkArray = new Link[size];

        for (int j = 0; j < size; j++) {
            // generate a random number
            int n = (int) (java.lang.Math.random() * 99);
            Link newLink = new Link(n); // create a new link
            linkArray[j] = newLink; // put it in the array
        }

        // display array contents
        System.out.print("Unsorted array: ");
        for (int j = 0; j < size; j++) {
            System.out.print(linkArray[j].dData + " ");
        }
        System.out.println();

        // create a new list initialized with the array
        SortedList theSortedList = new SortedList(linkArray);

        for (int j = 0; j < size; j++) {
            // move links from the list to the array
            linkArray[j] = theSortedList.remove();
        }

        // display array contents
        System.out.print("Sorted Array: ");
        for (int j = 0; j < size; j++) {
            System.out.print(linkArray[j].dData + " ");
        }
        System.out.println();
    } // end main()
} // end class ListInsertionSortApp
